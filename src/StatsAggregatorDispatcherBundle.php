<?php

namespace VerisureLab\Library\StatsAggregatorDispatcher;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use VerisureLab\Library\StatsAggregatorDispatcher\DependencyInjection\StatsAggregatorDispatcherExtension;

class StatsAggregatorDispatcherBundle extends Bundle
{
    public function getContainerExtension(): StatsAggregatorDispatcherExtension
    {
        return new StatsAggregatorDispatcherExtension();
    }
}