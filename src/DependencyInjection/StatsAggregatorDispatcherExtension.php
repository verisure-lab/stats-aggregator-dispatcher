<?php

namespace VerisureLab\Library\StatsAggregatorDispatcher\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\ConfigurableExtension;

class StatsAggregatorDispatcherExtension extends ConfigurableExtension
{
    protected function loadInternal(array $mergedConfig, ContainerBuilder $container): void
    {
        $container->setParameter('verisure_lab.stats_aggregator_dispatcher.dsn', $mergedConfig['dsn']);
        $container->setParameter('verisure_lab.stats_aggregator_dispatcher.queue_name', $mergedConfig['queue_name']);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('services.yaml');
    }
}