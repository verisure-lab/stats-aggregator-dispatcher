<?php

namespace VerisureLab\Library\StatsAggregatorDispatcher\Dispatcher;

use Enqueue\Client\Config;
use Enqueue\Sqs\SqsDestination;
use Enqueue\Sqs\SqsMessage;
use Enqueue\Sqs\SqsProducer;
use Interop\Queue\Context;
use VerisureLab\Library\StatsAggregatorDispatcher\Commands;
use VerisureLab\Library\StatsAggregatorDispatcher\UseCase\DispatchableUseCase;

class StatsDispatcher
{
    /**
     * @var SqsProducer
     */
    private $producer;

    /**
     * @var SqsDestination
     */
    private $destination;

    public function __construct(SqsProducer $producer, SqsDestination $destination)
    {
        $this->producer = $producer;
        $this->destination = $destination;
    }
    
    public function dispatch(string $id, string $type, array $data): void
    {
        $messageData = ['id' => $id, 'type' => $type, 'data' => $data];
        $this->producer->send($this->destination, new SqsMessage(json_encode($messageData, JSON_THROW_ON_ERROR)));
    }
}